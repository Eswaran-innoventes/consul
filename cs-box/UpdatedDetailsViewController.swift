//
//  UpdatedDetailsViewController.swift
//  cs-box
//
//  Created by Innoventes Technologies on 10/03/16.
//  Copyright © 2016 Innoventes Technologies. All rights reserved.
//

import UIKit

class UpdatedDetailsViewController: UIViewController {
    
    @IBOutlet weak var UpdatedDetailsTextField: UITextView!
    
    
    var companyName : String = ""
    var contactPerson : String = ""
    var Department : String = ""
    var mobileNo : String = ""
    var emailAddr : String = ""
    var landline : String = ""
    var productSno : String = ""
    var selectedProduct : String = ""
    var Rating1 : String = ""
    var Comment1 : String = ""
    var warranty_registration_id : String = ""
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.setHidesBackButton(true, animated: false)
        
        UpdatedDetailsTextField.text = "Product: \(selectedProduct)\n" + "Serail No: \(productSno)\n" + "Company Name: \(companyName)\n" + "Contact Person:\(contactPerson)\n" + "Department: \(Department)\n" + "Mobile No: \(mobileNo)\n" + "Email Address: \(emailAddr)\n" + "Landline: \(landline)"
        

    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        self.view.endEditing(true);
    }
}
