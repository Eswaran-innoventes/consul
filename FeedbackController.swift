//
//  FeedbackController.swift
//  cs-box
//
//  Created by Innoventes Technologies on 02/03/16.
//  Copyright © 2016 Innoventes Technologies. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import SQLite



class FeedbackController: UIViewController {
    
    @IBOutlet weak var textField: UITextField!

    @IBOutlet weak var Slider: UISlider!
    @IBOutlet weak var Comment: UITextView!
    var store2 : String = ""
    var details : String = ""
    
    //Store Personal Details screen information
    var warranty_registration_id = ""
    var companyName : String = ""
    var contactPerson : String = ""
    var Department : String = ""
    var mobileNo : String = ""
    var emailAddr : String = ""
    var landline : String = ""
    var productSno : String = ""
    var selectedProduct : String = ""
    var Rating1 : String = ""
    var Comment1 : String = ""

    var productDetails: [String] = []
    var ProductSerial: [String] = []
    


    override func viewDidLoad() {
        super.viewDidLoad()
        
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    func textFieldShouldReturn(textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        self.view.endEditing(true);
    }
    
    @IBAction func sliderValueChanged(sender: UISlider) {
         let currentValue = Int(sender.value)
         textField.text = "\(currentValue) "
        
    }
    
    @IBAction func Submit(sender: AnyObject) {
        details = store2 + " Product scale value: "+textField.text!+" Customer Feedback: "+Comment.text!
        Comment1 = Comment.text!
        Rating1 = textField.text!
        let ratingValue = Int(Slider.value)
        let parameters = [
            "product": selectedProduct,
            "serial_number": productSno,
            "name": contactPerson,
            "company_name": companyName,
            "mobile": mobileNo,
            "email": emailAddr,
            "rating": ratingValue,
            "comment": Comment1,
            "landline": landline,
            "status": "inprogress",
            "department": Department
        ]
        
        
        let headers = [
            "X_REST_USERNAME": "wsuser",
            "X_REST_PASSWORD": "wspass"
        ]
        
        Alamofire.request(.GET, "http://www.innoventestech.in/consul/backoffice/api/warrantyRegistration", headers: headers, encoding: .JSON).responseJSON() { (response) in
            
            switch response.result {
            case .Success(let data):
                let json = JSON(data)
                let title = json["data"]["warrantyRegistration"];
                for (_, value) in title {
                    let values: AnyObject = value["serial_number"].description
                    self.ProductSerial.append(values as! String)
                }
            case .Failure(let error):
                print("Request failed with error: \(error)")
            }
        }
        if(!(self.ProductSerial.contains(self.productSno))){
            Alamofire.request(.POST, "http://www.innoventestech.in/consul/backoffice/api/warrantyRegistration", parameters: parameters as? [String : AnyObject], headers: headers, encoding: .JSON)
            .responseJSON { response in
                switch response.result {
                case .Success(let data):
                    
                    let json = JSON(data)
                    let title = json["data"]["warrantyRegistration"][0]["warranty_registration_id"].description
                    self.warranty_registration_id.appendContentsOf(title)
                    
                    if let _ = SD.executeChange("INSERT INTO warranty_details(serial_number, product) VALUES (?, ?)", withArgs: [self.productSno, self.selectedProduct]) {
        
                    } else {

                    }
                    self.performSegueWithIdentifier("UpdateDetails", sender: sender)
                    
                case .Failure(let error):
                    let alert = UIAlertController(title: "Oops!!", message:"\(error)..", preferredStyle: .Alert)
                    alert.addAction(UIAlertAction(title: "Okay.", style: .Default) { _ in })
                    self.presentViewController(alert, animated: true){}
                }
        }
    }
        else {
            let alert = UIAlertController(title: "Oops!!", message:"Serial Number Already Exists...", preferredStyle: .Alert)
            alert.addAction(UIAlertAction(title: "Okay.", style: .Default) { _ in })
            self.presentViewController(alert, animated: true){}
        }
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        
        if let UpdateDetails = segue.destinationViewController as? UpdateDetails{
            UpdateDetails.productSno = productSno
            UpdateDetails.selectedProduct = selectedProduct
            UpdateDetails.companyName = companyName
            UpdateDetails.contactPerson = contactPerson
            UpdateDetails.Department = Department
            UpdateDetails.mobileNo = mobileNo
            UpdateDetails.emailAddr = emailAddr
            UpdateDetails.landline = landline
            UpdateDetails.Rating1 = textField.text!
            UpdateDetails.Comment1 = Comment.text!
            UpdateDetails.warranty_registration_id = warranty_registration_id
        }
    }
}
